#!/usr/bin/python3

import sys
try:
    import gitlab
except ImportError:
    print("Install gitlab: python3 -m pip install --user python-gitlab")
    sys.exit(1)
try:
    import tomllib
    toml_mode = 'rb'
except ImportError:
    import toml as tomllib
    toml_mode = 'r'
import os
import datetime
import keyring
import argparse
import getpass


try:
    import keyring.backends.libsecret
    keyring.set_keyring(keyring.backends.libsecret.Keyring())
except:
    pass



# support routine, gets the configuration and the application token

def get_config():

    # try to load the configuration files
    try:
        cfname = os.environ.get("HOME", '')+'/.config/dueca-git-token.conf'
        with open(cfname, toml_mode) as cf:
            conf = tomllib.load(cf)
            user = conf['General']['user']
            host = conf['General']['host']
            groups = conf['General']['groups']
    except FileNotFoundError:
        print(f"Cannot find config file at {cfname}")
        sys.exit(1)
    # except tomllib.TOMLDecodeError:
    #     print(f"Config file {cfname} has invalid format")
    #     sys.exit(1)
    except Exception as e:
        print(f"Problem reading config file {cfname}: {e}")
        sys.exit(1)

    return host, user, groups


def get_token(host: str, user: str):

    # once this worked, try to get the secret token
    try:
        token = keyring.get_password(f'accesstoken-{host}', user)

    except Exception as e:
        print(f"Cannot get gitlab master application token, {e}")
        sys.exit(1)

    # when not found, ask the user directly
    if token is None:

        token = getpass.getpass("Enter your GitLab access token:")

    # when nothing, complain and inform
    if not token:
        print('Cannot find or get gitlab master application token'
              f' accesstoken-{host}, user {user}')
        sys.exit(1)

    return token


def make_tokens(netid, host, groups, gl, days=365, writegroups=None):

    # find out when this should expire
    expiry = (datetime.date.today() + datetime.timedelta(days=days)).isoformat()

    # resulting file text
    res = [
    f"""# DUECA project environment variable definitions for user {netid}
# Created on {datetime.date.today().isoformat()}, valid until {expiry}
# The keys in this file are strictly personal, and not to be distributed

# An environment variable for accessing our DUECA policies
export DUECA_POLICIES=https://gitlab.tudelft.nl/dueca-ae-cs-policies/common/-/raw/master/index.xml

# Public projects, can simply access and clone
export DAPPS_GITROOT_pub=https://github.com/dueca/

# Your supervisor will give you "normal" access to the project you will be
# working on

# The locations for projects that might be needed (borrowed) are listed
# below. Using the environment variables as defined here, gives you the
# required keys to access these projects read-only""" ]

    # name for the tokens
    tname = f'gitlab+dueca-token-{netid}'

    if writegroups is None:
        writegroups = []

    # list the groups
    for group in gl.groups.list(iterator=True):
        if group.name in groups:

            try:
                if group.name in writegroups:
                    scopes = ['write_repository']
                    print(f"Adding write permissions for group {group.name}")
                else:
                    scopes = ['read_repository']
                # create the token
                ntoken = group.deploytokens.create({
                    'name': netid,
                    'scopes': scopes,
                    'username': tname,
                    'expires_at': expiry
                })

                # collect the stuff
                tag = group.name.split('-')[-1]
                res.append(
                    f'export DAPPS_GITROOT_{tag}=https://{tname}:{ntoken.token}@{host}/ae-cs-dueca-{tag}/'
                )
            except Exception as e:
                print(f"Failed to create token {tname} in group {group.name}: {e}")
                print(scopes)
    return '\n'.join(res)


# Command-line interface
parser = argparse.ArgumentParser(
    description="Manage access keys for DUECA repositories")
parser.add_argument(
    '--verbose', action='store_true',
    help="Verbose run with information output")
parser.add_argument(
    '--host', type=str,
    help="Override host name from config")
parser.add_argument(
    '--group', type=str, action='append',
    help="Manually specify group(s), overriding list from config")
parser.add_argument(
    '--user', type=str,
    help="Override your username from config")

subparsers = parser.add_subparsers(help='commands', title='Commands')


class SetMyToken:

    @classmethod
    def args(cls, subparsers):
        parser = subparsers.add_parser('token',
            help="Add my application token")
        parser.add_argument(
            '--username', type=str, default='default',
            help="Your username/netid for the application token")
        parser.add_argument(
            '--token', type=str,
            help='Application token')
        parser.set_defaults(handler=SetMyToken)

    def __call__(self, ns):

        if ns.host and ns.username:
            host, user = ns.host, ns.username
        else:
            host, user, _ = get_config()

        if not ns.token:
            ns.token = getpass.getpass("Enter your GitLab access token:")

        keyring.set_password(f'accesstoken-{host}', user, ns.token)


# initialize
SetMyToken.args(subparsers)


class MakeConfig:

    @classmethod
    def args(cls, subparsers):
        parser = subparsers.add_parser('config',
            help="Create a new config")
        parser.add_argument(
            '--overwrite', action='store_true',
            help="Overwrite an existing config"
        )
        parser.set_defaults(handler=MakeConfig)

    def __call__(self, ns):
        ns.host = ns.host or 'gitlab.tudelft.nl'
        ns.user = ns.user or 'anonymous'
        ns.groups = ns.group or [
            f'AE-CS-DUECA-{t}'
            for t in ('base', 'yard', 'active', 'archive')]
        gline = ', '.join([f'"{g}"' for g in ns.groups])
        cnffile = os.environ.get('HOME', '') + '/.config/dueca-git-token.conf'
        if os.path.exists(cnffile) and not ns.overwrite:
            print(f"There is still a file at {cnffile}, please remove it first")
        with open(cnffile, 'w') as cf:
            cf.write("[General]\n")
            cf.write(f'user="{ns.user}"\n')
            cf.write(f'host="{ns.host}"\n')
            cf.write(f'groups=[ {gline} ]\n')


MakeConfig.args(subparsers)


class ListTokens:

    @classmethod
    def args(cls, subparsers):
        parser = subparsers.add_parser('list',
            help='List existing tokens')
        parser.set_defaults(handler=ListTokens)

    def __call__(self, ns):

        if not (ns.host and ns.group and ns.user):
            host, user, groups = get_config()
            ns.host = ns.host or host
            ns.user = ns.user or user
            ns.groups = ns.group or groups

        mytoken = get_token(ns.host, ns.user)
        gl = gitlab.Gitlab(url=f'https://{ns.host}', private_token=mytoken)
        gl.auth()

        for group in gl.groups.list(iterator=True):
            if group.name in ns.groups:
                print(f"Group {group.name}:")
                for dt in group.deploytokens.list(iterator=True):
                    print(f"  {dt.name}, {dt.expires_at}, {dt.scopes}")


ListTokens.args(subparsers)


class RemoveTokens:

    @classmethod
    def args(cls, subparsers):
        parser = subparsers.add_parser('remove',
            help='Remove a user\'s token')
        parser.add_argument('netid',
            help='Netid or nick of the student')
        parser.set_defaults(handler=RemoveTokens)

    def __call__(self, ns):

        if not (ns.host and ns.group and ns.user):
            host, user, groups = get_config()
            ns.host = ns.host or host
            ns.user = ns.user or user
            ns.groups = ns.group or groups

        mytoken = get_token(ns.host, ns.user)
        gl = gitlab.Gitlab(url=f'https://{ns.host}', private_token=mytoken)
        gl.auth()

        for group in gl.groups.list(iterator=True):
            if group.name in ns.groups:
                for dt in group.deploytokens.list(iterator=True):
                    if dt.name == ns.netid:
                        dt.delete()
                        print(f"Deleted {dt.name} from {group.name}")


RemoveTokens.args(subparsers)


class CreateTokens:

    @classmethod
    def args(cls, subparsers):
        parser = subparsers.add_parser('create',
            help='Create a set of tokens')
        parser.add_argument('netid', type=str,
            help='Netid of the student')
        parser.add_argument('--write', action='append',
            help='Specify group (or ending) where user will receive'
                 'write permissions')
        parser.add_argument('--filename', type=str,
            help='File to store the variables script')
        parser.add_argument('--days', type=int, default=365,
            help="Number of days validity")
        parser.set_defaults(handler=CreateTokens)


    def __call__(self, ns):

        if not (ns.host and ns.group and ns.user):
            host, user, groups = get_config()
            ns.host = ns.host or host
            ns.user = ns.user or user
            ns.groups = ns.group or groups

        mytoken = get_token(ns.host, ns.user)
        gl = gitlab.Gitlab(url=f'https://{ns.host}', private_token=mytoken)
        gl.auth()

        contents = make_tokens(ns.netid, ns.host, ns.groups, gl, ns.days, ns.write)

        if ns.filename and not os.path.exists(ns.filename):
            with open(ns.filename, 'w') as fn:
                fn.write(contents)
        else:
            print(contents)


CreateTokens.args(subparsers)


# run the parser on the arguments
pres = parser.parse_args(sys.argv[1:])

# extract the handler class
try:
    hclass = pres.handler
except AttributeError:
    parser.print_usage()
    sys.exit(-1)

# create and run the handler
handler = hclass()
handler(pres)
