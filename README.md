# DUECA-GIT-TOKEN

A small utility script to create personalized "deploy" tokens for DUECA 
git repositories

## Use

- Create a default configuration file

      $> dueca-git-token --host gitlab.tudelft.nl --user mynetid config

- To run the script's actions, you need a gitlab application token. Obtain 
  this by opening your gitlab menu: "Preferences" - "Access Tokens", and add
  a personal access token there. Check "api" scope. 
  
- Once you have a gitlab access token, add it to the system's
  default keyring:
  
      $> dueca-git-token token --username mynetid
	  Enter your GitLab access token: SECRETS!
	  
- List all the (deploy) tokens issued on the groups in your config

	  $> dueca-git-token list

- Remove deploy tokens

	  $> dueca-git-token remove gitlab+dueca-token-johnny

- Create deploy tokens, save the settings to a file

	  $> dueca-git-token create johnny --filename johnny.txt --days 365
	  
