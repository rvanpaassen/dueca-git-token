cmake_minimum_required(VERSION 3.10)
enable_language(C)
include(GNUInstallDirs)
project(dueca-git-token VERSION 1.0)

install(PROGRAMS src/dueca-git-token.py
  DESTINATION ${CMAKE_INSTALL_BINDIR}
  RENAME dueca-git-token)
